package com.opentrophy.kitchensink;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.opentrophy.client.OTAchievement;
import com.opentrophy.client.OTScore;
import com.opentrophy.sdk.OpenTrophy;

public class KitchenSinkActivity extends Activity {
    private static final String LOG_TAG = "OTKC";

    private TextView localPlayerText;
    private Button loginBtn;
    private Button logoutBtn;
    private Button dashboardBtn;
    private EditText leaderboardIdText;
    private Button leaderboardBtn;
    private Button achievementsBtn;
    private EditText leaderboardIdForScoreText;
    private EditText scoreText;
    private Button scoreBtn;
    private EditText achievementIdForProgressText;
    private EditText progressText;
    private Button progressBtn;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        String key = "519f771c2721470fd9000005";//app1
        String secret = "test";

        OpenTrophy.getInstance().init(this, key, secret);

        localPlayerText = (TextView) findViewById(R.id.localPlayerText);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        dashboardBtn = (Button) findViewById(R.id.dashboardBtn);
        leaderboardIdText = (EditText) findViewById(R.id.leaderboardIdText);
        leaderboardBtn = (Button) findViewById(R.id.leaderboardBtn);
        achievementsBtn = (Button) findViewById(R.id.achievementsBtn);
        scoreText = (EditText) findViewById(R.id.scoreText);
        leaderboardIdForScoreText = (EditText) findViewById(R.id.leaderboardIdForScoreText);
        scoreBtn = (Button) findViewById(R.id.scoreBtn);
        progressText = (EditText) findViewById(R.id.progressText);
        achievementIdForProgressText = (EditText) findViewById(R.id.achievementIdForProgressText);
        progressBtn = (Button) findViewById(R.id.progressBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "signin click!");
                OpenTrophy.getInstance().showSignin();
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "signout click!");
                OpenTrophy.getInstance().signout(null);
            }
        });

        dashboardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "dashboard click!");
                OpenTrophy.getInstance().showDashboard();
            }
        });

        leaderboardBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "leaderboard click!");
                int leaderboardId = Integer.parseInt(leaderboardIdText.getText().toString());
                OpenTrophy.getInstance().showLeaderboard(leaderboardId);
            }
        });

        achievementsBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "achievements click!");
                OpenTrophy.getInstance().showAchievements();
            }
        });

        scoreBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "score click!");
                String leaderboardId = leaderboardIdForScoreText.getText().toString();
                int value = Integer.parseInt(scoreText.getText().toString());
                OpenTrophy.getInstance().reportScore(leaderboardId, value, null);
            }
        });

        progressBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "progress click!");
                String achievementId = achievementIdForProgressText.getText().toString();
                int value = Integer.parseInt(progressText.getText().toString());
                OpenTrophy.getInstance().reportProgress(achievementId, value, null);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
